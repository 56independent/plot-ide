function testCode(input, idealOutput, actualOutput, title, description) { // TODO: Find a way to abstract away this function into a seperate file.
    //console.log("Testing \"" + title + "\", with description \"" + description + "\".")
    if (idealOutput == actualOutput){
        console.log("Test " + title + " worked! (With input \"" + input + "\".)")
        return true
    } else {
        console.error("Test " + title + " failed. (Input \"" + input + "\" should have gave output \n\"" + idealOutput + "\"\nbut instead gave\n\"" + actualOutput + "\"\n.)") 
        return false
    }
}

function testObjectArray(objects) {
    for (let object of objects) {
        testCode(object.input, object.idealOutput, object.actualOutput, object.title, object.description)
    }
}

let tests = [
    /*
    Template:
    {
        "input":"",
        "idealOutput":"",
        "actualOutput": null,
        "title": "",
        "description": ""
    },
    */
    {
        "input":"(print 'hello, world!')",
        "idealOutput":"hello, world!",
        "actualOutput": null,
        "title": "print to RPN, single quote",
        "description": "Tests that code that prints 'Hello, World!' works"
    },
    {
        "input":"(print \"hello, world!\")",
        "idealOutput":"hello, world!",
        "actualOutput": null,
        "title": "print to RPN, double quotes",
        "description": "Tests that code that prints 'Hello, World!' works"
    },
    {
        "input":"(print (+ 2 3) (* 4 3) (+ 2 (* 3 4)))",
        "idealOutput":"",
        "actualOutput": null,
        "title": "Nested input to print to RPN",
        "description": "Tests that code that prints 'Hello, World!' works"
    },
    {
        "input":"(if (> 2 3) {(print \"this is impossible\")} {(print \"3 is greater then 2\")})",
        "idealOutput":"3 is greater then 2",
        "actualOutput": null,
        "title": "Non-nested if statements",
        "description": "Test that if statements are properly converted"
    }
]

//console.log("testing")

for (i = 0; i < tests.length; i++) {
    //console.log(tests[i])
    tests[i].actualOutput = runPlotCode(tests[i].input, ["test", "slib"]) // Find a way to specify the function
}

testObjectArray(tests)


