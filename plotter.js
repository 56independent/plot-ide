/*
Defining backboneJS libraries is as simple as doing 

globalEnvironments.x = {}

Where x is the library you want.
*/

INTERPRETER = {
    VARIABLE_DEFINE: "var",
    FUNCTION_DEFINE: ["function", "f", "lambda"],
    STRING_L: ["\"", "'", "«"],
    STRING_R: ["\"", "'", "»"],
    DATA_REGEXES: [/^'.*?'$/, /^".*?"$/, /^«.*?»$/, /\d+\.\d*/, /^\{.*\}$/],
    CODE_BLOCK: ["{", "}"],
    LINE_COMMENT: ";",
    INCLUDE: "include"
}

state = {
    "libraries":["slib", "test"]
}

function stripCodeBraces(text) {
    textArray = text.split("")
    
    if (textArray[0] == INTERPRETER.CODE_BLOCK[0]) textArray[0] = ""
    if (textArray[textArray.length-1] == INTERPRETER.CODE_BLOCK[1]) textArray[textArray.length-1] = ""
}

// Define a basic environment
globalEnvironment = {
    "test":{
      '+': (a, b) => a + b,
      '-': (a, b) => a - b,
      '*': (a, b) => a * b,
      '/': (a, b) => a / b,
      'print': (...args) => {
        console.log(...args)
        return args
      },
      "if": (...args) => {
        console.log(args) 
        return args
      },
      ">":(a, b) => (a > b) ? 1 : 0,
    },
    "slib":{
        "neg": (a) => -a,
        "+": (...args) => {
            let argsThing = args
            let number = 0
            
            console.log(argsThing, argsThing.length)
            
            for (let i = 0; i < argsThing.length; i++) {
                number += Number(argsThing[i])
            }
            
            console.log(number)
            return number
        },
        "while": (a, b) => {while (a) {runPlotCode(stripCodeBraces(b), state.libraries)}},
        "runString": (a) => {runPlotCode(stripCodeBraces(a), state.libraries || ["slib"])}
    }
};

function isData(token) {
    return INTERPRETER.DATA_REGEXES.some(regex => regex.test(token));
}

function tokenise(code) { // Take in the code and return an AST; IE: make each exression, such as (+ 2 3) into an array of lexed objects. Each array represents a statement.
    if (! code) return false 

    const codeByChar = code.split("")
    const blockBegginers = [].concat(INTERPRETER.CODE_BLOCK[0], INTERPRETER.STRING_L) // TODO: I wonder if this is the best way

    // TODO: Allow codeblocks to nest infinitely

    let blob = ""
    let tokens = []
    let finalTokens = [[0]]
    let depth = 0
    let escaped = false
    let block = false
    let blockEnder = true
    let codeBlockDepth = 0
    
    for (const char of codeByChar) {
        //console.log(finalTokens, tokens, blob, depth, char)
        if (block) {
            //console.log(char + " is in a block", blockEnder, blob)
            
            //if (char == INTERPRETER.CODE_BLOCK[0]) codeBlockDepth += 1
            //else if (char == INTERPRETER.CODE_BLOCK[1]) codeBlockDepth = codeBlockDepth-1

            if (!escaped && char == blockEnder && codeBlockDepth == 0) {
                block = false
                blob += char
                tokens.push(blob)
                blob = ""
            } else {
                blob += char
            }
            
            
            if (char == "\\") escaped = true
            else escaped = false
        } else if (char != "(" && char != ")") {
            //console.log(char + " is in a non-block")

            if (blockBegginers.includes(char)) {
                blob += char
                
                isCodeBlock = blockBegginers.indexOf(char) == 0
                blockEnder = (isCodeBlock) ? INTERPRETER.CODE_BLOCK[1] : INTERPRETER.STRING_R[blockBegginers.indexOf(char)-1]
                block = true
            } else if (char == " ") {
                if (blob != "") tokens.push(blob)
                blob = ""
            } else {
                blob += char
            }
        } else if (char == "(") {
            //console.log(char + " is in open bracket")
            finalTokens.push(tokens)
            depth += 1
            tokens = [Number(depth)]
        } else if (char == ")") {
            //console.log(char + " is in a close bracket", tokens, finalTokens)
            depth = depth-1
            tokens.push(blob)
            finalTokens.push(tokens)
            
            blob = ""
            tokens = [Number(depth)]
        }
    }
    
    lastTokens = []
    
    for (const token of finalTokens) { // A little filter. Caused by code earlier adding two bad items.
        if (token.length > 1) {
            lastTokens.push(token)
        }
    }
    
    return lastTokens
}

//console.log(tokenise("function"), [[], [0, "function"]])
//console.log(tokenise("(print \"hello, world!\" (+ 2 3)"), [[], [1, "print", "\"hello, world!\""] [2, "+", 2, 3]])
//console.log(tokenise("(if (> 2 3) {(print \"this is impossible\")} {(print \"3 is greater then 2\")})"))

/*function handleFuntionDefinitions(tokens, userDefinedFunctions={}) {
    /*
    * Create `definedFunctions`, an object.
    * Iterate over all the tokens
    * For each one where the function name matches `function` (from `INTERPRETER.FUNCTION_DEFINE`)
        * Take the first argument and add it as a name for a new object
        * Take the second and use it as the 
    * For each one which matches
        * Take the arguments
        * Use some find and replace, and for each argument there, replace "a\d" with the argument ID
        * If there aren't enough arguments, throw an error
        * Put the new code through the tokeniser and then use that minilist instead of the current element
        * Continue increasing i through this thing to handle nested function calls
    /
    let depth
    
    for (let i = 0; i < tokens.length; i++) {
        token = tokens[i]
        if (token[1] == INTERPRETER.FUNCTION_DEFINE) { // Remember, the 0th index is the "depth".
            if (token.length < 3+1) {
                throw new Error("Parsing token " + token + " failed due to a lack of arguments.")
            }
            functionName = token[2]
            functionBody = tokenise(token[3])
            
            let args = 0
            
            for (token of functionBody) {
                if (!isData(token[1]) && token[1].test(/a\d+/)) {
                    args += 1
                }
            }
            
            userDefinedFunctions[functionName] = {
                body: functionBody,
                expectedArgsNumber: args,
            }
        } else if (userDefinedFunctions[token[1]]) { // Should the function match a defined one, we can go read forth until we go to a depth higher then ours and collect the arguments we see long the way.
            let depth = token[0]
            let foundDepth = depth
            
            let args = []
            
            for (let o = i; foundDepth >= depth && o < tokens.length; o++) {
                args.push(tokens[o])
            }
            
            // TODO: Add code to replace the function body an values with the actual arg names and then replace the tokens used for the function with the body tokens. Yes, i know it's a macro, not a function, but i'm tired of writing this code. Goodnight.
        }
    }
}*/

let definedFunctions = {}

function execute(tokenList, includedLibraries, globalEnvironments=globalEnvironment) { // From https://chat.openai.com/share/90fbbe23-81e1-4502-9781-fd0e4f721898 
 const dataRegexes = INTERPRETER.DATA_REGEXES;

  function executeFunction(name, args, environment) {
    /*
    * [At token-execute time]
    * Read the second element of the token, the function name
    * If it is within the array INTERPRETER.FUNCTION_DEFINE
        * Take the first argument of the function and use it as the value for `functionName`
        * Take the argument after that and use it as the value for `codeToRun`
    * If it is a valid key for object `definedFunctions`
        * Put `definedFunctions[object]` into `called`
        * Find the number of arguments accepted for `called`
        * If it's 0, simply return the return value 
        * If it's above 0
            * For each of the other things with the token
            * Find the value of it
            * If we reach the end of the token
                * If the depth of the token ahead is larger:
                    * Execute that token with all the local scope we already have and add it to a stack
                * If the depth is equal
                    * For each thing in the token
                        * If it's data, add it to the stack
                        * If not, simply run the rest of the token
            * Take all the arguments we now have
            * Run the function
    */

    if (name == INTERPRETER.FUNCTION_DEFINE) {
        
    }
  
    if (environment.hasOwnProperty(name)) {
      const func = environment[name];
      const evaluatedArgs = args.map(arg => isData(arg) ? arg : executeToken(arg, environment));
      return func(...evaluatedArgs);
    } else if (name != "") {
      throw new Error(`Function '${name}' not found in the environment`);
    }
  }

  function executeToken(token, environments, resultArray, previousResult) { // TODO: Find a way to use ResultArray
    if (Array.isArray(token)) {
      const [depth, ...rest] = token;
      
      let currentEnvironment = {};
      
      if (includedLibraries) { 
        for (let i = 0; i < includedLibraries.length; i++){
            currentEnvironment = {...currentEnvironment, ...globalEnvironments[includedLibraries[i]]}
        }
      } else currentEnvironment = globalEnvironments["test"]
      
      console.log(rest)

      if (isData(rest[0])) {
        return rest[0]; // Data, return as is
      } else if (rest.slice(1).length != 0) {
        const functionName = rest[0];
        let args = rest.slice(1);

        console.log(args, previousResult, rest.slice(1), rest.slice(1).length != 0)
        if(previousResult) args.push(previousResult)

        return executeFunction(functionName, args, currentEnvironment);
      }
    } else {
      return token; // Literal value, return as is
    }
  }

  let resultArray = []
  let result = "";
  for (const token of tokenList) {
    result = executeToken(token, globalEnvironments, resultArray, result);
    if (result) resultArray.push(result)
    console.log(resultArray)
  }

  return result;
}

function runPlotCode(input, libraries) {
    state.libraries = libraries
    
    //console.log(input, libraries)
    
    return execute(tokenise(input), libraries, globalEnvironment);
}

// Quicktests
testCode = [
    "(runString {(print \"This is a user-ran codeblock!\")})",
    "(print 'Hello, world!')",
    "(print 'Hello, world!' ( + 2 3))",
    "(print (+ 1 1 1 2))",
]

for (let codelet of testCode) {
    console.log(runPlotCode(codelet, ["test, slib"]) + " from " + codelet)
}
