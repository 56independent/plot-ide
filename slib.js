function actuallyRunFunction(functionName, data, libraryName="slib") { // TODO: deconstruct as per the documentation
    if (libraryName != "slib"){
        return false
    }

    let newData = data

    let returnValue
    let output

    switch (functionName) {
        // Base functions
        case "+":
            returnValue = 0
            for (dataElement of data) {
                if (dataElement !== undefined){
                    returnValue += Number(dataElement)
                }
            }
            break

        case "neg":
            for (dataElement of data) {
                if (dataElement !== undefined ) {
                    returnValue.push(-Number(dataElement))
                }
            }
            break

        case "while":
            findFirstResult = () => {new plotInterpreter(data[1], "pwot", ["slib"], true).returnValue} // TODO: Support all languages

            while (findFirstResult()[0] != 0) {
                codeRan = new plotInterpreter(data[2], "pwot", ["slib"], true)
            }

            returnValue = codeRan.returnValue
        case "if":
            findFirstResult = () => {new plotInterpreter(data[1], "pwot", "slib", true).returnValue}

            if (findFirstResult[0]%2 == 0) {
                plot = new plotInterpreter(data[1], "pwot", ["slib"], true)
            } else {
                plot = new plotInterpreter(data[2], "pwot", ["slib"], true)
            }

            returnValue = plot.returnValue

            break
        // List processing
        case "index":
            returnValue = data[0][data[1]]
            break
        case "length":
            returnValue = data[0].length
            break
        case "split":
            returnValue = []
            returnValue.push(data[0].slice(0, data[1]+1))
            returnValue.push(data[0].slice(data[1]+1, data[0].length))
            break
        case "merge":
            returnValue = []
            for (i=0;i<data.length;i++) returnValue.push(data[i])
            break

        // More then core
        case "++":
        case "*":
            returnValue = 1
            for (dataElement of data) {
                returnValue = returnValue*Number(dataElement)
            }
            break

        case "-":
            for (dataElement of data) {
                returnValue -= Number(dataElement)
            }
            break

        case "--":
        case "/":
            for (dataElement of data) {
                returnValue =  returnValue/Number(dataElement)
            }
            break

        case "..":
        case "concat":
            returnValue = ""
            console.log(data)
            for (dataPoint of data){
                //console.log(dataPoint)
                returnValue += dataPoint.toString()
            }
            break

        case "time":
            let time = new Date
            returnValue = time.toISOString()
            break

        case "epoch":
            returnValue = Date.now()
            break

        case "print":
            output = []
            console.log(data)
            for (dataPoint of data) {
                console.log(dataPoint)
                output.push(dataPoint) // There was a REALLY frustrating error with output appearing multiple times because i accidentally was returning data directly, instead. This comment is left in memory.
            }
            break

        default:
            return false
            //console.log("Function `" + functionName + "` not defined")
    }

    return [returnValue, output]
}