const body = document.body;

// Output container
const outputContainer = document.createElement('div');
outputContainer.id = 'output';
body.appendChild(outputContainer);

// Input container
const inputContainer = document.createElement('div');
inputContainer.id = 'input-container';
body.appendChild(inputContainer);

// Input text field
const inputField = document.createElement('input');
inputField.type = 'text';
inputField.id = 'input';
inputField.placeholder = 'Type your JavaScript code here';
inputField.style.flexGrow = '1';
inputField.style.padding = '5px';
inputContainer.appendChild(inputField);

// Run button
const runButton = document.createElement('button');
runButton.id = 'run-btn';
runButton.innerHTML = 'Run';
runButton.style.marginLeft = '5px';
runButton.style.padding = '5px 10px';
runButton.style.cursor = 'pointer';
runButton.addEventListener('click', runCode);
inputContainer.appendChild(runButton);

document.addEventListener('keyup', function (event) {
    if (event.key === 'Enter') {
        runCode();
    }
});

// JavaScript code execution function
function runCode() {
    const inputElement = document.getElementById('input');
    const outputElement = document.getElementById('output');

    try {
        const result = eval(inputElement.value);
        outputElement.innerHTML = `<b>Result:</b> ${result}`;
    } catch (error) {
        outputElement.innerHTML = `<b>Error:</b> ${error.message}`;
    }
}
