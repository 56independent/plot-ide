# Standard Library (`slib`)
The standard library is split into categories which achieve all manner of things.

It is imported by default in most implementations.

It is split into two main parts; `core` and `extensions`. `core` provides all the functionality required to become turing complete and capable of doing things, but `extensions` provides a large part of the functionality, mainly through functions defined in plot.

The power here is that without endangering the base, one can override exisiting functions in `extensions` to implement new functionality.

## Core
The core of the standard library is the purest and most minimalist functions to base the rest of the library off of.

#### Interpreter Functions
Whilst not implemented by the standard library, are included for completeness:

`(include module)`

Takes in a list of modules and imports them from the package manager.

`(function "name" {code})`

Defines a function of name `"name"` and makes it such that it will begin running `{code}` every time its called; any arguments within are simply called `an`, with `n` being an integer above 0.

#### Control flow

`(while {condition} {code})`

Runs `{code}` until condition is divisible by 2

`(if {condition} {code1} {code2})`

If `condition` is not divisible by 2, run `{code1}`, else run `{code1}`

#### List processing
Base list processing functions

`(index list index) -> index`

Get an element at a specific index in the list

`(length list) -> length`

Get the length of a specified list

`(split list index) -> (list1 list2)`

Split a list at specified index, with index being left-sticky

`(merge list1 list2) -> list`

Merge two lists (remember; the definition of a list is loose. They can be an empty set to a list of function calls) and return the final list

#### Maths
`(+ list)`

Adds all elements of the list together

`(neg list)`

Turns all elements of the list negative

## Extensions
### data
`(var "name" {value})`

Defines a variable of name `"name"` and gives it the return value of value. TODO: Can be defined as `(function "var" {(return a1)})` with no ill effects; move into the extensions part

### controlFlow
`(for var {iterate} {condition} {do})`

Take a variable `var` and run `do` and `iterate` until `condition` is not divisible by two

`(until {condition} {do})`

Keep on doing `do` until `condition` is not divisble by two.

`(forever {do})`

Do `do` infinitely

### maths
`(* list)` or `(++ list)`

Multiplies all elements of the list

`(^ list)` or `(+++ list)`

Powers all elements of the list

`(++++ list)`

Power-powrs all elements of the list (powers itself that many times)

`(- list)`

Subtracts each element of the list

`(/ list)` or `(-- list)`

Divides all elements of the list

`(rt list)` or `(--- list)`

Roots all elements of the list

`(---- list)`

Power-power-roots alll elements of the list (powers itself that many times)
