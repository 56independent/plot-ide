class storage { // TODO: I'm beggining to doubt the utility of a class which is basically just a storage bucket and an updating function.
    constructor () {
        this.storage = {
            settings: {
                enabledLibs: ["slib"],
                currentFile: "main.plt"
            },
            files: [
                ["main.plt", `Code editor not properly initalised; please refresh, or failing that, contact the developers`]
            ]
        }
    }

    updateFile(code, filename=this.storage.settings.currentFile) {
        for (let i = 0; i < GLOBAL_STORAGE.storage.files.length; i++){
            if (GLOBAL_STORAGE.storage.files[i][0] == filename) {
                GLOBAL_STORAGE.storage.files[i][1] = code
                break
            }
        }
    }

    getFileByName(name) {
        for (const file of this.storage.files) {
            if (file[0] == name) {
                return file
            }
        }
    }
}

GLOBAL_STORAGE = new storage()
