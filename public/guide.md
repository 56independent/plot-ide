# Plot: Getting Started
**Note** This guide has been written with the default plot IDE in mind. Other IDEs and development environments are not considered.

Plot is a remarkably simple but powerful language. It has been designed to turn learning the language into more of simply learning a standard library and less of memorising masses of syntax. It's also very modular with many layers of abstraction to suit any task.

In Plot, there are only four types of data:

* The string, as denoted by `"`
* The number, which can either be a float or integer
* The late-interpretation code block, which is a piece of code surrounded by `{` and `}`. This code won't be interpreted unless it's passed as data. 
* The returner/function, which is a little reference to a value or a way to modify values

In fact, the string and the number can be thought of as functions which return their own value. 

## A guided first program
Now then, let's discuss building programs. Plot is quite a simple language. You simply shove data and functions together.

So, write this function:

`(print "Hello, World!")`

Thinking in terms of plot, what we have done is set up the function `print` to take in the value `"Hello, World!"` and then act upon that, in this case, outputting "Hello, World" to the screen.

Now then, what happens if we want to put two values together? We can use the `concat` function or `..`, the alias of it. Let's put together the ISO 8601 timestamp of the current date and the string `" is the current time"`

`(.. time " is the current time")`

Now then, we've concatenated two values. On its own, it's quite useless, so let's output the final result:

`(print .. time " is the current time")`

We have now successfully outputted the current time. Also note that we have also constructed a so-called "function chain". This is where each function transfers data to the left, carrying the return value with it. This differs from LISP, in which every function along the chain would need to have a seperate bracket. When handling the function chain, the data "cascades" left as it hits each function. What print effectively "sees" would simply be "[ISO 8601 date string] is the current time", and none of the functions before it, as they have already been turned into return values.

If it helps, you may compare this to the equivalent JavaScript:

`let time = new Date; console.log(time.toISOString() + " is the current time")`

Or the equivalent pseudocode:

`print(time("iso8601") .. " is the current time")`

A returner can be thought of as a "grain" of plot functionality. Everything you interact with in plot will be formed of these grains. In fact, numbers and strings can be thought of as grains that return themselves

## Creating grains
Plot itself has two hardcoded grains;  `function` and `import`. Their functionality, instead of being part of the modular function system, is baked into the interpreter.

To use `var`, which defines variables (which are really a shorthand for functions which merely return), simply follow this syntax:

`(var "name" value)`

Which is the same as this JS:

`let name = "value"`

For functions, follow this syntax:

`(function "name" {code})`

Within the code argument, if you wish to add arguments, use "an", where n represents an integer. For example, to implement an adder, use this:

`(function "add" {(+ a1 a)})`

This is equivalent to this JS code:

```js
function add(a1, a2) {
    return a1+a2
}
```

Imports are ways to take in either a file or a named module and use the functions defined there. None of the code which isn't inside a `function` call is brought in.

For example, to import named libraries `pquery` and `scheme`, as well as the user-written `utils.plt` file, do this:

`(import "pquery" "scheme" "utils.plt")`

The difference between a "named library" and a "user-written plot file" comes down to the dialect the library is written in and the location of the plot file you're writing.

## Libraries; the heart of plot
Libraries provide all of the functionality of plot. Each library has its own functions, for which more data can be obtained by reading their specific documentation. However, put simply:

* `slib`, or Standard Library - defines the base functionality which is reusable. It creates features which brings plot to the execution level of other languages.
* `pqeury`, or Plot-Query - A plot wrapper for the Jquery library, which allows building full GUIs. In the default editor, target `#head`.
* `scheme`, or Plot-scheme - A plot wrapper for Scheme, a LISP dialect. This provides a minimalist alternative to the `slib` library.

Libraries are controlled using the `import` keyword. Import in the order which is needed; later imports's functions override earlier imports. By default, it is expected that slib and all its standard extensions is included.

For example, to import `pquery` and then have the default `(click)` overriden by your own function, defined in `click.plt`, do this:

`(include "pquery" "click.plt")`

**Note** Remember to insert `.plt` for your-file based libraries, otherwise it'll look on the package manager's libraries!

## Dialects of plot
There are various dialects of plot. This section is ordered in the likelyhood the average begginer is likely to find them.

All dialects can cross-talk to such a degree a function defined in one dialect is accessible in another. 

### Informal Plot
Informal plot is the easiest plot dialect to think in and use. Simple write some code and the return/stout value is given back to the user. 

### Formal "Software" Plot
Formal plot is a dialect of plot focused on providing some form of structure to the code. It is intended for larger projects in which structure does well.

Every formal plot project sits inside its own folder with at least a `main.plt` and `config.plt` file. 

`config.plt` defines some base data:

```
(multivar
    ("name" "Project")
    ("techName" "project")
    ("description" "This project adds some new features")
    ("requredModules" "slib" "pquery")
)
```

From this, the project can then be imported into external codebases without the `.plt` extension to signfiy it is contained within a folder with the `config.plt`. The system interpreter should have a `path` list containing the paths of projects. Ensure your project is within it.

`main.plt` must have a `main` function, or the code will not run. Anything not in a function produces an error.

### FLARE-Plot
FLARE-Plot is a very strict language designed to reduce bugs in production code. Based off ideas of the SPARK language and 56independent's planned A-level EPQ Dissetation on reducing software bugs.

It has a very strong reccomendation to keep code granular and functions small, which allow the other components of FLARE-Plot to take over. It is a very type-strict language. 

In FLARE-Plot every function must have unit tests at the end that effectively cover the function and all possible edgecases sent to it. At the beggining, an ASCII version of z-notation describes how the function works. Also required is documentation of the function.

This increased strictness is designed to prevent bugs at the cost of code-writing velocity. For example:

```
; Z-notation here
(function "multiply" (args "a1" "number" "a2" "number") (doc "Takes in two numbers and multiplies them using a for loop"){
    (var "total" {0}) ; Type implied here to be number.
    (xTimes a2 {
        (var "total" {(+ total a1)})
    })
    (return total)
}) (= (multiply 2 3) 6) (errorPresent (multiply 2 "hello"))
```

Please note the significant difference from the informal plot equivalent:

```
(function "multiply" {
    (var "total" {0}) ; Type implied here to be number.
    (xTimes a2 {
        (var "total" {(+ total a1)})
    })
    (return total)
})
```

Although the FLARE-Plot is more verbose and redundancy-heavy, it's harder for a bug to slip in.

FLARE-Plot, under the surface, is really just a very strict linter and transpiles down to Plot before interpretation. 

FLARE-Plot can be thought of as the level above Formal Plot; it still needs the structure but it then adds bug-reducing tactics on top of it. 

### Shell Plot
Shell plot is a specific kind of plot. It uses RPN and the `&` keyword to combine arguments should there be more then 2. `(+ 1 2)` is equivalent to `2 1 +` and `(+ 4 3 2 1)` is equivalent to `1 & 2 & 3 & 4 +`. 

This plot dialect is mainly designed for environments in which it's more likely Plot will be used in a REPL as an interface to a system. It's not very common outside of the hypothetical PlotOS operating system.

## The power of plot
Now that you're aqquainted with the language, we can now discuss its power.

Plot, given its simplicity, is very extendable. Making your own grains allows you to effectively change the syntax and functionality of the language. This allows plot to change to fit any usecase. For example, if you were working from a shell environment and you wanted plot to help write a script, you could define a few bindings to allow plot to expand into the niche:

``(function "ls" `(listFiles a1)`)``

This power of plot is a reoccurent theme. It's called "the layer-cake of abstraction". It extends to contain the whole language. The `*` function is not defined by the interpreter; it's an addition operator repeated a set amount of times. In fact, the arithmetic side of the interpreter-defines plot standard library is only the addition and negation operators. 

