var baseTheme = {
    foreground: '#F8F8F8',
    background: '#2D2E2C',
    selection: '#5DA5D533',
    black: '#1E1E1D',
    brightBlack: '#262625',
    red: '#CE5C5C',
    brightRed: '#FF7272',
    green: '#5BCC5B',
    brightGreen: '#72FF72',
    yellow: '#CCCC5B',
    brightYellow: '#FFFF72',
    blue: '#5D5DD3',
    brightBlue: '#7279FF',
    magenta: '#BC5ED1',
    brightMagenta: '#E572FF',
    cyan: '#5DA5D5',
    brightCyan: '#72F0FF',
    white: '#F8F8F8',
    brightWhite: '#FFFFFF'
};
// vscode-snazzy https://github.com/Tyriar/vscode-snazzy
var otherTheme = {
    foreground: '#eff0eb',
    background: '#282a36',
    selection: '#97979b33',
    black: '#282a36',
    brightBlack: '#686868',
    red: '#ff5c57',
    brightRed: '#ff5c57',
    green: '#5af78e',
    brightGreen: '#5af78e',
    yellow: '#f3f99d',
    brightYellow: '#f3f99d',
    blue: '#57c7ff',
    brightBlue: '#57c7ff',
    magenta: '#ff6ac1',
    brightMagenta: '#ff6ac1',
    cyan: '#9aedfe',
    brightCyan: '#9aedfe',
    white: '#f1f1f0',
    brightWhite: '#eff0eb'
};
var isBaseTheme = true;

var term = new window.Terminal({
    fontFamily: '"Cascadia Code", Menlo, monospace',
    theme: baseTheme,
    cursorBlink: true,
    allowProposedApi: true
});
term.open(document.getElementById('terminal'));
term.writeln("Plot live REPL. For help, write `help`.");

// As per https://chat.openai.com/share/6255edeb-55e8-42f1-9c2c-5f625c8a3520

// Initialize variables
let inputBuffer = '';

function promptCode() {
    term.write("\n>>> ")
}

let state
let command

term.onData(e => {
    switch (e) {
      case '\u0003': // Ctrl+C
        term.write('^C');
        promptCode();
        break;
      case '\r': // Enter
        command = '';
        promptCode();
        break;
      case '\u007F': // Backspace (DEL)
        // Do not delete the prompt
        if (term._core.buffer.x > 2) {
          term.write('\b \b');
          if (command.length > 0) {
            command = command.substr(0, command.length - 1);
          }
        }
        break;
      default: // Print all other characters for demo
        if (e >= String.fromCharCode(0x20) && e <= String.fromCharCode(0x7E) || e >= '\u00a0') {
          command += e;
          term.write(e);
        }
    }
  });

// Function to check if parentheses and curly brackets are balanced
function areBracketsBalanced(input) {
    const stack = [];
    const openingBrackets = ['(', '{'];
    const closingBrackets = [')', '}'];
    
    for (let char of input) {
        if (openingBrackets.includes(char)) {
            stack.push(char);
        } else if (closingBrackets.includes(char)) {
            const lastOpeningBracket = stack.pop();
            const correspondingOpeningBracket = openingBrackets[closingBrackets.indexOf(char)];
            
            if (lastOpeningBracket !== correspondingOpeningBracket) {
                return false; // Mismatched brackets
            }
        }
    }
    
    return stack.length === 0; // Stack should be empty for balanced brackets
}

let innards = false

// This function is human-generated
function handleCode(code) {
    console.log(code)

    // This is a hack to remove the 9 letters of undefined if it's at the start. TODO: Fix
    if(/^undefined/.test(code)) {
        for (let i = 0; i < 9; i++) {
            code = code.split("").slice(1).join("")
        }
    }

    console.log(code)

    args = code.split(" ") || [code]

    console.log(args, args[0] == "slib-function")

    if (args[0] == "slib-function"){ // TODO: Prevent unnecesary code duplication
        fetch("./standard library.md")
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok'); // Ok ChatGPT, that text is not ok LMAO
            }
            return response.text();
        })
        .then(markdownText => {
            if (!args[1]) {
                
                    term.writeln(markdownText)
            } else {
                    let desiredFunction = args[1]

                    let matchThing = "^.*?`\(" + desiredFunction + ".*?\n\n.*"
                    let matchGex = new RegExp(matchThing, "gm")

                    let match = markdownText.match(matchGex)
                    console.log(match)
                    term.write(match)
            }
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error);
        });

        return
    } else {
        console.log(code)
        switch (code) {
            case "help":
                term.writeln("")
                term.writeln("showInternals - Print out the tokens and their lexed version as stringified JSON")
                term.writeln("plot-help - Print out the `guide.md` document for learning about plot")
                term.writeln("slib-function - Take a function name and return its docs, or failing that, just print out the entire standard library")
                break

            case "showInternals":
                innards = !innards
                break

            case "plot-help":
                fetch("guide.md")
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok'); // Ok ChatGPT, that text is not ok LMAO
                    }
                    return response.text();
                })
                .then(markdownText => {
                    term.write(markdownText)
                })
                .catch(error => {
                    console.error('There was a problem with the fetch operation:', error);
                });
                return

            default:
                plot = new plotInterpreter(code, "pwot", ["slib"], true, state)
                console.log(plot)
                //plot.runCode(code, ["slib"])

                term.write("\n")
                state = plot.returnValye

                console.log(plot.output, plot.returnValue)
                output = plot.output.flat(Infinity).join("\n") || plot.returnValue.flat(Infinity).join("\n") || "Null"
                console.log(output)

                console.log(output)

                if (innards) {
                    term.writeln(JSON.stringify(plot.tokens))
                    term.writeln(JSON.stringify(plot.lexed))
                    term.writeln("")
                }

                term.writeln(output)
                console.log(output)
        }
    }
}

promptCode()

// Testing reasons
// handleCode('(print "hello, world!")')
// handleCode('(+ 2 3)')
// handleCode("help")
// handleCode("slib-function if")
// handleCode("slib-function")
// promptCode()
