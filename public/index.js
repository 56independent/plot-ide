// Get editor working
let codeEditor = $("#editor")

editor = ace.edit("editor")
editor.setTheme("ace/theme/monokai")
editor.session.setMode("ace/mode/lisp")
editor.setFontSize(20)

URLCode = getFromURL("code")
if (URLCode) {
    editor.setValue(URLCode)
}

function changeEditorFile(filename){
    console.log(filename)
    GLOBAL_STORAGE.storage.settings.currentFile = filename
    editor.setValue(GLOBAL_STORAGE.getFileByName(filename)[1])
}

function getCodeChange(){
    code = editor.getValue()

    //putInURL("code", code)
    GLOBAL_STORAGE.updateFile(code)
    return code
}

codeEditor.change(() => {
    updateThings()
})

