# Pquery
Pquery is a Jquery wrapper for Plot.

## Usage
To use pquery, simply use jquery functions in plot. If you're familiar with jquery, this should come naturally. `#head` is the div that will show, so edit that. For example, to put a message on screen, simply use:

`(append ($ "#head") (append ($ "<p>") (text "Hello, World!)))`

This is equivelnt to the following JS

`$("#head").append($("<p>").text("Hello, World"))`
