# Scheme (Plot Bindings)
The _Scheme (Plot Bindings)_  (SPB) library is a series of plot bindings to allow full usage of scheme, albeit with the caveats below. SPB aims to be fully backwards compatible with Scheme (Guile implementation).

https://web-artanis.com/scheme.html

