function output(data){
    $("#outputter").empty().append($("<p>").text(data)) // TODO: Accomidate more output types
}

function getLibs() {
    var selectedValues = [];
    $('input[name="languages[]"]:checked').each(function() {
        selectedValues.push($(this).val());
    });
    
    console.log(selectedValues)
    return selectedValues
}

$("#run").click(() => { // Ok, so plot files are ordinarily .plt. For RPN plot, use .tlp
    getCodeChange()
    code = GLOBAL_STORAGE.storage.files
    finalCode = ""
    
    //console.log(code)
    
    for (const file of code) {
        //console.log(file)
        if (file[0].endsWith(".tlp")) {
            finalCode += file[1]
        } else { // Non-extension and ".plt" files are both considered pwot files
            let string = ""
            let plot2RPN = new plotInterpreter("", "pwot", [], false)
            console.log(file[1])
            string = plot2RPN.turnToRPN(file[1])
            console.log(string)
            finalCode += string
        }
    }
    
    console.log(finalCode)
    updateThings()
    
    interpreted = new plotInterpreter(finalCode, "płot", getLibs())
    console.log(interpreted.output)
    
    output(interpreted.output)
})

function putProjectIntoUrl(){
    putInURL("project", JSON.stringify(GLOBAL_STORAGE.storage))
}

$("#save").click(() => {
    let string = JSON.stringify(GLOBAL_STORAGE.storage)
    downloadText(string, "plotIDESave.json")
    putInURL("project", string)
})

function readURLProjectString(){
    let string =  getFromURL("project")
    if (string){
        GLOBAL_STORAGE.storage = JSON.parse(string)
        return true
    } else {
        return false
    } 
}

$("#load").click(() => {
    let string = getFromURL("project")
    if (!readURLProjectString()) {
        GLOBAL_STORAGE.storage = openFileDialogAndGetContents()
    }
})

// TODO: We could potentially use 2D array of guide names and id names, and match them up.
function openAndParseMarkdownFile(filename) {
    fetch(filename)
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok'); // Ok ChatGPT, that text is not ok LMAO
        }
        return response.text();
    })
    .then(markdownText => {
        var converter = new showdown.Converter();
        var html = converter.makeHtml(markdownText);
        document.getElementById("outputter").innerHTML = html;
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });
}

$("#gettingStarted").click(() => {
    openAndParseMarkdownFile("guide.md")
})

$("#slibDoc").click(() => {
    openAndParseMarkdownFile("standard library.md")
})

$("#pqueryDoc").click(() => {
    openAndParseMarkdownFile("pquery.md")
})

$("#schemeDoc").click(() => {
    openAndParseMarkdownFile("scheme.md")
})

$("#run").click()

function handleNewOrder(initiate=false){ // TODO: Improve name and make initiate's goal clearer; leaving it false handles a new order, leaving it true handles new files? Seriously, this needs to be fixed.
    let files = GLOBAL_STORAGE.storage.files
    
    if (initiate) {
        $("#files").empty()
        for (let i = 0; i < files.length; i++){
            const filename = files[i][0]
            $("#files").append(
                $("<div>")
                    .attr("data-id", files[i][0])
                    .attr("id", files[i][0])
                    .text(files[i][0])
                    .attr("class", "boxy p-1")
                    .click(()=> {
                        changeEditorFile(filename)
                    })
            )
        }
    } else {
        // What we're doing here is taking the order of files and then sorting the GLOBAL_STORAGE to fulfill the new order. This is done by taking the file, finding its source code and arranging the new array.
        var fileNamesOrdered = $('#files').sortable('toArray');
        
        var finalList = []

        console.log(fileNamesOrdered)
        for (const name of fileNamesOrdered) {
            finalList.push(GLOBAL_STORAGE.getFileByName(name))
        }
    }
}

$("#files").sortable({
    onSort: handleNewOrder,
})

handleNewOrder(true)

// TODO: Improve this code by making it less repetitive
// TODO: Improve the UX with the prompt by making it keep attention near the right; use a text-field
$("#add-top").click(() => {
    var filename = prompt("Enter filename; for RPN plot, use `.tlp`, for LISP-plot, use `.plt`:")
    GLOBAL_STORAGE.storage.files = [filename, "; Enter code here"].concat(GLOBAL_STORAGE.storage.files)
    handleNewOrder(true)
})
$("#add-bottom").click(() => {
    var filename = prompt("Enter filename; for RPN plot, use `.tlp`, for LISP-plot, use `.plt`:")
    GLOBAL_STORAGE.storage.files.push([filename, "; Enter code here"])
    handleNewOrder(true)
    console.log(GLOBAL_STORAGE)
})

readURLProjectString()
console.log(GLOBAL_STORAGE.storage)
handleNewOrder(true)

function updateThings(){
    handleNewOrder()
    putProjectIntoUrl()
    getCodeChange()
    editor = ace.edit("editor")
    editor.setValue(GLOBAL_STORAGE.getFileByName(GLOBAL_STORAGE.storage.settings.currentFile)[1])
    getCodeChange()
}
