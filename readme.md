# Plot Environment and Plot Libraries
This repo contains the plot environment and plot interpretation libraries.

# Plot interpreting library
To get the JS Library for interpreting plot, simply do:

```
wget https://gitlab.com/56independent/plot-ide/-/raw/main/plotter.js
```

And to get the standard library, go do:

```
wget https://gitlab.com/56independent/plot-ide/-/raw/main/slib.js
```

The plot interpreter turns LISP-plot (colloquially called pwot in writing and plisp in speaking) into RPN plot (colloquially called płot in writing and fabuła in speaking) before then executing that through JS. Full API docs are yet to be written.

You can also choose to write directly into RPN plot, too.

The interpreter is a seperate library and can be taken for your own use by copying `plotter.js`. The standard library used by FOSS MM is available in `runFunctions.js` but it is reccomended to use the standard standard library instead, which is yet to be implemented.

The class structure is as follows:


* Class `plotInterpreter`
* `interpret (string, languageType)`
* `constructor (string, languageType, libraries, test)`

`string` is the code to interpret.

`langageType` is the type of plot to use. For LISP-Plot either use `lispplot` or `pwot`. For RPN plot, use either `płot` or `fabuła`. `test` is optional and should be ignored unless you're testing the plot interpreter itself.

`libraries` is an array of libraries to use in plot. The names ae as defined in their individual function files; read the first comment in `plotter.js` for information regarding creating function files.

# Plot IDE
The plot IDE is a webapp designed to make plot programming as streamlined as possible. It offers the following features:

* Writing and interpreting plot code in any environment capable of running the web-app
* Handling multiple files of plot code as arranged by a table
* Importing and exporting plot environments as JSON
* Accessing information on plot and its libraries
* Being accessed anywhere with internet

To use the IDE, simply visit the website, either at the [official link](https://plot-ide-56independent-ddf58a757c55d2717a37c165f9abddbceef41f09.gitlab.io/public/), or the [shortened version](https://bit.ly/plot-ide) (at bit.ly/plot-ide) in any modern web browser.

Within the IDE you will be able to access each feature from the main screen. At the top, you can access the running and load/save functionality for projects. On the right, you can access documentation. Below them both is the output screen, below that is the code editor, and below that the libraries selector.

Each of these small groups form part of the cohesive Plot-IDE™ experience.
